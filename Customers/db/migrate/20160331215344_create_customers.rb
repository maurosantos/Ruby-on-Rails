class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :name
      t.string :email
      t.date :aniversario
      t.text :obs

      t.timestamps null: false
    end
  end
end
