json.array!(@customers) do |customer|
  json.extract! customer, :id, :name, :email, :aniversario, :obs
  json.url customer_url(customer, format: :json)
end
